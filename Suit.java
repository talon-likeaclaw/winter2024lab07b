public enum Suit {
    CLUBS(0.1),
    DIAMONDS(0.2),
    HEARTS(0.4),
    SPADES(0.3);

    private double score;

    Suit(double score) {
        this.score = score;
    }

    public double getScore() {
        return score;
    }
}