public class Card {
	
	// fields
	private Suit suit;
	private Rank rank;
	
	// constructor
	public Card(Suit suit, Rank rank) {
		this.suit = suit;
		this.rank = rank;
	}
	
	// getters
	public Suit getSuit() {
		return this.suit;
	}
	
	public Rank getRank() {
		return this.rank;
	}
	
	// toString()
	public String toString() {
		return this.rank + " of " + this.suit;
	}
	
	// Calculate Score of card.
	public double calculateScore() {
		double score = 0.0;
		double suit = 0.0;
		// Determine decimal rank with suit rank.
		score = this.rank.getScore() + this.suit.getScore();
		return score;
	}
}